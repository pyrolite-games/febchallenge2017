﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(ScenarioGenerator))]
public class GeneratorEditor : Editor {

	int MIN_POINTS = 5;
	int MAX_POINTS = 50;


	public override void OnInspectorGUI () {
		DrawDefaultInspector ();

		ScenarioGenerator myGenerator = (ScenarioGenerator)target;

		if (myGenerator.minPoints < MIN_POINTS) {
			myGenerator.minPoints = MIN_POINTS;
		}

		if (myGenerator.maxPoints > MAX_POINTS) {
			myGenerator.maxPoints = MAX_POINTS;
		}

	}

}
