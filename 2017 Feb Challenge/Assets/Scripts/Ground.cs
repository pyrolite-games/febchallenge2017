﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(LineRenderer))]
[RequireComponent (typeof(BoxCollider2D))]
public class Ground : MonoBehaviour {

	LineRenderer myLineRenderer;
	BoxCollider2D myBoxCollider;


	void Awake () {
		myLineRenderer = GetComponent<LineRenderer> ();
		myBoxCollider = GetComponent<BoxCollider2D> ();
	}


	public void ResetCollider () {
		Vector3 positionA = myLineRenderer.GetPosition (0);
		Vector3 positionB = myLineRenderer.GetPosition (1);
		RecenterCollider (positionA, positionB);
		ResizeCollider (positionA, positionB);
	}

	private void RecenterCollider (Vector3 posA, Vector3 posB) {
		myBoxCollider.transform.position = Vector3.Lerp (posA, posB, 0.5f);
		myBoxCollider.transform.rotation = Quaternion.Euler (new Vector3 (0f, 0f, AngleBetweenVector2 (posA, posB)));
	}

	private void ResizeCollider (Vector3 posA, Vector3 posB) {
		myBoxCollider.size = new Vector3 (
			Vector3.Distance (posA, posB),
			myBoxCollider.size.y
		);
	}

	private float AngleBetweenVector2 (Vector2 vec1, Vector2 vec2) {
		float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
		return Vector2.Angle (Vector2.right, vec2 - vec1) * sign;
	}


	void OnCollisionEnter2D (Collision2D coll) {
		Debug.Log ("Colliding with: " + coll.gameObject.name);
	}


}
