﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Propeller))]
public class Fuel : MonoBehaviour {

	[SerializeField] float maxFuel = 100f;
	[SerializeField] float currentFuel;
	public float CurrentFuel { get { return currentFuel;} }
	bool outOfFuel = false;


	void Start () {
		currentFuel = maxFuel;
	}


	public void ConsumeFuel (float amount) {
		currentFuel -= amount;
		if (currentFuel <= 0f) {
			outOfFuel = true;
		}
	}

	public bool IsOutOfFuel () {
		return outOfFuel;
	}

}
