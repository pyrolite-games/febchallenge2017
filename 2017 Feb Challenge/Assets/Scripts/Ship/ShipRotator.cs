﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipRotator : MonoBehaviour {

	/// <value>The ship's target angle. It will slowly rotate towards it.</value>
	[SerializeField] public float TargetAngle { get; set; }
	/// <summary>
	/// Gets the rotation speed.
	/// </summary>
	/// <value>The rotation speed.</value>
	public float RotationSpeed { get { return rotationSpeed;} }
	private float rotationSpeed = 30f;


	void Start () {
		TargetAngle = transform.rotation.eulerAngles.z;
	}


	void Update () {
		transform.rotation = Quaternion.RotateTowards (transform.rotation, GetTargetRotation (TargetAngle), RotationSpeed * Time.deltaTime);
	}


	private Quaternion GetTargetRotation (float angle) {
		return Quaternion.Euler (new Vector3 (0f, 0f, angle));
	}

}
