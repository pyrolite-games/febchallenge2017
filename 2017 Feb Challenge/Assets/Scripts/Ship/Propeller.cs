﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(Fuel))]
public class Propeller : MonoBehaviour {

	readonly int MIN_FORCE = 0;
	readonly int MAX_FORCE = 10;
	readonly float FUEL_USAGE_FACTOR = 2.5f;
	float IMPULSE_FORCE_FACTOR;

	Rigidbody2D myRigidbody;
	Fuel myFuelTank;
	public Transform propellerSprite;

	// A force of 5 will make the ship stable in the air
	[SerializeField] int currentForce;


	void Awake () {
		myRigidbody = GetComponent<Rigidbody2D> ();
		myFuelTank = GetComponent<Fuel> ();
	}

	void Start () {
		IMPULSE_FORCE_FACTOR = 25f /-Physics2D.gravity.y;
	}


	void FixedUpdate () {
		if (currentForce > 0 && !myFuelTank.IsOutOfFuel ()) {
			myRigidbody.AddForce (myRigidbody.transform.up * IMPULSE_FORCE_FACTOR * Time.fixedDeltaTime * currentForce, ForceMode2D.Force);
			myFuelTank.ConsumeFuel (currentForce * FUEL_USAGE_FACTOR * Time.fixedDeltaTime);
		}
		if (!myFuelTank.IsOutOfFuel ()) {
			AdjustFire ((float)currentForce / MAX_FORCE);
		} else {
			AdjustFire (0f);
		}
	}


	// Adjust the sprite scale and position to visually represent the current propeller force
	private void AdjustFire (float scale) {
		propellerSprite.localScale = new Vector3 (scale, scale, 0f);
		propellerSprite.localPosition = new Vector3 (0f, Mathf.Lerp (-0.31f, -0.53f, scale), 0f);
	}


	/// <summary>
	/// Use the ship's propeller to impulse the ship vertically
	/// The higher the force, the higher the fuel usage
	/// </summary>
	/// <param name="force">The propulsion force, from 0 (none) to 10 (max)</param>
	public void SetForce (int force) {
		currentForce = Mathf.Clamp (force, MIN_FORCE, MAX_FORCE);
	}

	/// <summary>
	/// Increases the force in <paramref name="increment"/> points.
	/// </summary>
	/// <param name="increment">The N points.</param>
	public void IncreaseForce (int increment) {
		SetForce (currentForce + increment);
	}

	/// <summary>
	/// Decreases the force in <paramref name="decrement"/> points.
	/// </summary>
	/// <param name="decrement">The N points.</param>
	public void DecreaseForce (int decrement) {
		SetForce (currentForce - decrement);
	}


	public int GetCurrentForce () {
		return currentForce;
	}

}
