﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomGravity : MonoBehaviour {

	float customGravityForce = -1.2f;


	void Awake () {
		Physics2D.gravity = new Vector2 (0f, customGravityForce);
	}

}
