﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(ShipRotator))]
[RequireComponent (typeof(Propeller))]
[RequireComponent (typeof(Fuel))]
public class ShipController : MonoBehaviour {

	Rigidbody2D myRigidbody;
	Collider2D myCollider;
	ShipRotator myRotator;
	Propeller myPropeller;
	Fuel myFuel;


	void Awake () {
		myRigidbody = GetComponent<Rigidbody2D> ();
		myCollider = GetComponent<Collider2D> ();
		myRotator = GetComponent<ShipRotator> ();
		myPropeller = GetComponent<Propeller> ();
		myFuel = GetComponent<Fuel> ();
	}


	// EDIT FROM HERE
	void Start () {
		
	}


	void Update () {
		
	}

}
