﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshRenderer))]
public class GroundPainter : MonoBehaviour {

	MeshFilter meshFilter;
	MeshRenderer meshRenderer;

	float verticalBounds;
	float horizontalBounds;


	void Awake () {
		meshFilter = GetComponent<MeshFilter> ();
		meshRenderer = GetComponent<MeshRenderer> ();
	}


	public void GenerateMesh (Vector3[] vertices) {
		verticalBounds = ScenarioGenerator.bounds [0].y - ScenarioGenerator.bounds [2].y;
		horizontalBounds = ScenarioGenerator.bounds [1].x - ScenarioGenerator.bounds [0].x;
		Triangulate (vertices);
	}


	private void Triangulate (Vector3[] points) {
		// Use the triangulator to get indices for creating triangles
		Triangulator tr = new Triangulator (points);
		int[] indices = tr.Triangulate ();

		// Create the Vector3 vertices
		Vector3[] vertices = new Vector3[points.Length];
		for (int i = 0; i < vertices.Length; i++) {
			vertices [i] = new Vector3 (points [i].x, points [i].y, 0);
		}

		// Create the mesh
		Mesh msh = new Mesh ();
		msh.vertices = vertices;
		msh.triangles = indices;
		RecalculateUVs (msh);
		msh.RecalculateNormals ();
		msh.RecalculateBounds ();

		// Set up game object with mesh;
		meshFilter.mesh = msh;
		meshRenderer.sharedMaterial = meshRenderer.material;
	}

	private void RecalculateUVs (Mesh mesh) {
		Vector2[] newUV = new Vector2[mesh.vertices.Length];
		for (int i = 0; i < mesh.vertices.Length; i++) {
			newUV [i] = new Vector2 (
				(mesh.vertices [i].x + (horizontalBounds / 2)) / horizontalBounds,
				mesh.vertices [i].y / verticalBounds);
		}
		mesh.uv = newUV;
	}

}
