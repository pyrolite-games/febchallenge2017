﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioGenerator : MonoBehaviour {

	public static Vector3[] bounds;

	public Transform groundPoints;
	public Transform groundLines;
	public GameObject pointPrefab;
	public GameObject linePrefab;
	public bool useManualPoints;
	public int minPoints = 5;
	public int maxPoints = 15;
	public float minRelativeHeight = 0.1f;
	public float maxRelativeHeight = 0.5f;
	float minHeight;
	float maxHeight;

	Vector3 bottomLeftCorner;
	Vector3 bottomRightCorner;
	Vector3 topLeftCorner;
	Vector3 topRightCorner;

	List<Vector3> pointList;


	void Awake () {
		pointList = new List<Vector3> ();
	}


	void Start () {
		topLeftCorner = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, -Camera.main.transform.position.z));
		topRightCorner = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, -Camera.main.transform.position.z));
		bottomLeftCorner = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, -Camera.main.transform.position.z));
		bottomRightCorner = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, -Camera.main.transform.position.z));
		bounds = new Vector3[] { topLeftCorner, topRightCorner, bottomLeftCorner, bottomRightCorner };
		maxHeight = topLeftCorner.y * maxRelativeHeight;
		minHeight = (topLeftCorner.y - bottomLeftCorner.y) * minRelativeHeight;

		GenerateMap (useManualPoints);
		GetComponent<GroundPainter> ().GenerateMesh (pointList.ToArray ());
	}


	public void GenerateMap (bool customPoints) {
		if (!customPoints) {
			GeneratePoints ();
		}
		GenerateEdgePoints ();
		AddPointsToList ();
		GenerateGround ();
	}

	private void GeneratePoints () {
		ClearPoints ();
		int numberOfPoints = Random.Range (minPoints, maxPoints);
		float distanceBetweenCorners = bottomRightCorner.x - bottomLeftCorner.x;
		float minIncrement = distanceBetweenCorners / numberOfPoints * 0.5f;
		float maxIncrement = distanceBetweenCorners / numberOfPoints;
		float lastX = bottomLeftCorner.x;
		for (int i = 0; i < numberOfPoints; i++) {
			GameObject point = Instantiate (pointPrefab, groundPoints, false);
			float nextX = Random.Range (lastX + minIncrement, lastX + maxIncrement);
			point.transform.position = new Vector3 (nextX, Random.Range (minHeight, maxHeight), 0f);
			lastX = point.transform.position.x;
		}
	}

	private void GenerateGround () {
		for (int i = 0; i < groundPoints.childCount - 1; i++) {
			GenerateLine (groundPoints.GetChild (i).position, groundPoints.GetChild (i + 1).position);
		}
	}

	private void GenerateEdgePoints () {
		GameObject firstPoint = Instantiate (pointPrefab, groundPoints, false);
		firstPoint.transform.SetAsFirstSibling ();
		firstPoint.transform.position = new Vector3 (bottomLeftCorner.x, Random.Range (minHeight, maxHeight), 0f);

		GameObject leftCornerPoint = Instantiate (pointPrefab, groundPoints, false);
		leftCornerPoint.transform.SetAsFirstSibling ();
		leftCornerPoint.transform.position = bottomLeftCorner;

		GameObject lastPoint = Instantiate (pointPrefab, groundPoints, false);
		lastPoint.transform.SetAsLastSibling ();
		lastPoint.transform.position = new Vector3 (bottomRightCorner.x, Random.Range (minHeight, maxHeight), 0f);

		GameObject rightCornerPoint = Instantiate (pointPrefab, groundPoints, false);
		rightCornerPoint.transform.SetAsLastSibling ();
		rightCornerPoint.transform.position = bottomRightCorner;
	}

	private void AddPointsToList () {
		pointList.Clear ();
		for (int i = 0; i < groundPoints.childCount; i++) {
			pointList.Add (groundPoints.GetChild (i).position);
		}
	}

	private void GenerateLine (Vector3 posA, Vector3 posB) {
		GameObject line = Instantiate (linePrefab, groundLines, false);
		line.transform.localPosition = new Vector3 ();
		line.transform.localRotation = new Quaternion ();

		LineRenderer renderer = line.GetComponent<LineRenderer> ();
		renderer.SetPosition (0, posA);
		renderer.SetPosition (1, posB);

		line.GetComponent<Ground> ().ResetCollider ();
	}


	private void ClearPoints () {
		for (int i = 0; i < groundPoints.childCount; i++) {
			DestroyImmediate (groundPoints.GetChild (i).gameObject);
		}
	}

}
